---
# To start a new cluster, set this to 'true' with:
# ansible-playbook playbook.yml -e "k3s_cluster_init=true"
# WARNING: Starting a new cluster can destroy your old one!
k3s_cluster_init: false

# Set this to 'true' with ansible-playbook -e 'k3s_debugging_enabled=true'
# to reveal the output of commands this role executes.
k3s_debugging_enabled: false

# Location of the main configuration file
k3s_conf_file: '/etc/rancher/k3s/config.yaml'

# Location of the cluster shared secret token
k3s_token_file: '/etc/rancher/k3s/token'

# Location of registries and mirrors configuration
# See https://docs.k3s.io/installation/private-registry
k3s_registries_conf_file: '/etc/rancher/k3s/registries.yaml'

# Architectures supported by k3s
k3s_architectures_supported:
  - 'x86_64'
  - 'aarch64'
  - 'armhf'
  - 'armv7l'
  - 'armv8l'
  - 's390x'

# Individual k3s releases (https://github.com/k3s-io/k3s/releases)
k3s_release_1_28_10_arm64:
  version: 'v1.28.10+k3s1'
  url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.10%2Bk3s1/k3s-arm64'
  checksum: 'sha256:ecf15ca4391892c7d98e41803d29fa029a87983cea4a9b0cd49400b1a61714bd'
  airgap_images_url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.10%2Bk3s1/k3s-airgap-images-arm64.tar.zst'
  airgap_images_checksum: 'sha256:e0646442ec520bc655b4d41afd875bdffaf4ea5f61ab23e8cc7830b9e901825f'

k3s_release_1_28_10_amd64:
  version: 'v1.28.10+k3s1'
  url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.10%2Bk3s1/k3s'
  checksum: 'sha256:2e90c4e696899ef37be871ec04986edac5ecf145da3e70e772e6c61f10b5400c'
  airgap_images_url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.10%2Bk3s1/k3s-airgap-images-amd64.tar.zst'
  airgap_images_checksum: 'sha256:38b481fb649b5d962b9024f8248174303e4f6ada7f1d0746ee89618cd56fe86d'

k3s_release_1_28_9_arm64:
  version: 'v1.28.9+k3s1'
  url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.9%2Bk3s1/k3s-arm64'
  checksum: 'sha256:54c02ce93032348fe40446c2e8a9622209c11a64d9d752da153fabfa2e2be5c7'
  airgap_images_url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.9%2Bk3s1/k3s-airgap-images-arm64.tar.zst'
  airgap_images_checksum: 'sha256:7b1c356149f31b8201d8a46c7bd0f9fb8c32b913d39c55012d896ca124fa5e36'

k3s_release_1_28_9_amd64:
  version: 'v1.28.9+k3s1'
  url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.9%2Bk3s1/k3s'
  checksum: 'sha256:fc5c10513764a226eb7eca3d353e2a6c9da584713b6d11c8b5ded8cae8e8997e'
  airgap_images_url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.9%2Bk3s1/k3s-airgap-images-amd64.tar.zst'
  airgap_images_checksum: 'sha256:cc60f496b9c70349c742ea0e640f9ed205adf97b824a7eb8158ef01061d65f1b'

k3s_release_1_28_8_arm64:
  version: 'v1.28.8+k3s1'
  url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.8%2Bk3s1/k3s-arm64'
  checksum: 'sha256:c84c8430a2cdec150176ee740759ed8792e48b69eaf72460168b01711e65d304'
  airgap_images_url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.8%2Bk3s1/k3s-airgap-images-arm64.tar.zst'
  airgap_images_checksum: 'sha256:83e1bccfcb32af9128a2eb0e2d866750461d4719e0c6c05aa07b27221d6ec146'

k3s_release_1_28_8_amd64:
  version: 'v1.28.8+k3s1'
  url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.8%2Bk3s1/k3s'
  checksum: 'sha256:de4982e25e1dac2d7d41665dd77255fbbe60e5cb216dd1216322519cce2654b7'
  airgap_images_url: 'https://github.com/k3s-io/k3s/releases/download/v1.28.8%2Bk3s1/k3s-airgap-images-amd64.tar.zst'
  airgap_images_checksum: 'sha256:ee0a905985396aeddb379b9500845dd200bcde9fdc8c395f018608d831d41ed0'

# Mapping of ansible arch names to specific k3s releases
k3s_release_arch:
  x86_64: "{{ k3s_release_1_28_10_amd64 }}"
  aarch64: "{{ k3s_release_1_28_10_arm64 }}"

k3s_system_name: >-
  {% if k3s_name != ''
    %}k3s-{{ k3s_name }}{%
  else %}{%
    if k3s_is_server | bool %}k3s{% else %}k3s-agent{% endif %}{%
  endif %}

# Location of the SCRAM shell script
# KILLALL_K3S_SH
k3s_killall_sh: "{{ k3s_bin_dir }}/k3s-killall.sh"

# Location of the un-install script
# UNINSTALL_K3S_SH
k3s_uninstall_sh: "{{ k3s_bin_dir }}/{{ k3s_system_name }}-uninstall.sh"

# Location of the k3s service script
# FILE_K3S_SERVICE
k3s_service_file: >-
  {% if ansible_service_mgr | lower == 'systemd'
  %}{{ k3s_systemd_dir }}/{{ k3s_system_name }}.service{%
  else
  %}/etc/init.d/{{ k3s_system_name }}{% endif %}

# FILE_K3S_ENV
k3s_env_file: >-
  {% if ansible_service_mgr | lower == 'systemd'
  %}{{ k3s_systemd_dir }}/{{ k3s_system_name }}.service.env{%
  else
  %}/etc/rancher/k3s/{{ k3s_system_name }}{% endif %}

# Location of the service log file, when running under OpenRC
k3s_log_file: "/var/log/{{ k3s_system_name }}.log"

# Location to cache downloaded binaries and images, useful to reduce downloads
# and to setup air-gap environments
k3s_releases_cache_dir: '~/.ansible/k3s/'

# INSTALL_K3S_SYSTEMD_DIR
# Directory to install systemd service and environment files to
k3s_systemd_dir: /etc/systemd/system

# Run k3s_killall.sh before restarting k3s.
# It will clean-up k3s so thouroughly, that longhorn may not be able
# to run again until the host is actually rebooted.
# So don't enable this on production systems.
k3s_killall_on_restart: false
