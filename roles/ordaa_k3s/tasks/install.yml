---
- name: 'Create local download dir'
  become: false
  ansible.builtin.file:
    path: "{{ k3s_releases_cache_dir }}"
    state: directory
    mode: '0755'
  delegate_to: 'localhost'
  check_mode: false

- name: 'Download binary to local folder'
  become: false
  ansible.builtin.get_url:
    url: "{{ k3s_release.url }}"
    dest: "{{ k3s_releases_cache_dir }}/{{ k3s_release.url | basename }}"
    checksum: "{{ k3s_release.checksum }}"
    mode: '0644'
  # run_once: true # <-- this cannot be set due to multi-arch support
  delegate_to: 'localhost'
  check_mode: false

- name: 'Download air-gap images to local folder'
  when:
    - 'k3s_release.airgap_images_url is defined'
    - 'k3s_release.airgap_images_checksum is defined'
  become: false
  ansible.builtin.get_url:
    url: "{{ k3s_release.airgap_images_url }}"
    dest: "{{ k3s_releases_cache_dir }}/{{ k3s_release.airgap_images_url | basename }}"
    checksum: "{{ k3s_release.airgap_images_checksum }}"
    mode: '0644'
  # run_once: true # <-- this cannot be set due to multi-arch support
  delegate_to: 'localhost'
  check_mode: false

- name: 'Propagate binary'
  become: true
  ansible.builtin.copy:
    src: "{{ k3s_releases_cache_dir }}/{{ k3s_release.url | basename }}"
    dest: "{{ k3s_bin_dir }}/k3s"
    owner: 'root'
    group: 'root'
    mode: '0755'
  register: '_k3s_propagate_binary'
  notify:
    - 'k3s_restart'

- name: 'Create binary symlinks'
  loop:
    - kubectl
    - crictl
    - ctr
  loop_control:
    loop_var: _k3s_symlink_bin
  become: true
  ansible.builtin.file:
    src: "{{ k3s_bin_dir }}/k3s"
    dest: "{{ k3s_bin_dir }}/{{ _k3s_symlink_bin }}"
    state: link
  register: '_k3s_create_symlinks'
  failed_when: >-
    _k3s_create_symlinks.failed
      and not(ansible_check_mode and _k3s_propagate_binary.changed)

- name: 'Propagate air-gap images'
  when:
    - 'k3s_release.airgap_images_url is defined'
    - 'k3s_release.airgap_images_checksum is defined'
  block:
    - name: 'Create air-gap images dir'
      become: true
      ansible.builtin.file:
        path: "{{ k3s_data_dir }}/agent/images/"
        state: directory
        owner: 'root'
        group: 'root'
        mode: '0755'

    - name: 'Propagate air-gap images'
      become: true
      ansible.builtin.copy:
        src: "{{ k3s_releases_cache_dir }}/{{ k3s_release.airgap_images_url | basename }}"
        dest: "{{ k3s_data_dir }}/agent/images/"
        owner: 'root'
        group: 'root'
        mode: '0644'
